# README #

### What is this repository for? ###

* This application allows to calculate square of right-angled triangle by inputting sizes of its sides. This project created for particular purpose.

### What can I do? ###

* Browse code and manage tests by opening solution (*.sln) file with Visual Studio 2012 (or higher).
* Launch executable file from TriangleSquare Debug or Release directory.
* Launch application from command line: state size lengths as consequent parameters.