﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleSquare
{
    public class Program
    {
        public const string SideIsLessThanOrZero = "Длина одной из сторон не является допустимой положительной величиной.";
        public const string TriangleIsNotRight = "Треугольник с заданными сторонами не является прямоугольным, так как квадрат длины гипотенузы не является суммой квадратов длин катетов.";

        static void Main(string[] args)
        {
            double a = 0.0;
            double b = 0.0;
            double c = 0.0;
            double square = 0.0;

            if (args.Length == 3)
            {
                double.TryParse(args[0], out a);
                double.TryParse(args[1], out b);
                double.TryParse(args[2], out c);
            }
            else
            {
                Console.Write("Введите длину первого катета: ");
                double.TryParse(Console.ReadLine(), out a);
                Console.Write("Введите длину второго катета: ");
                double.TryParse(Console.ReadLine(), out b);
                Console.Write("Введите длину гипотенузы: ");
                double.TryParse(Console.ReadLine(), out c);
            }
            try
            {
                square = GetTriangleSquare(a, b, c);
                Console.WriteLine("Площаль прямоугольного треугольника с катетами {0}, {1} и гипотенузой {2} = {3}.\n", a, b, c, square);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Ошибка при вычислении площади:\n{0}", ex.Message);
            }
            Console.WriteLine("Для выхода из программы нажмите любую клавишу.");
            Console.Read();
        }

        /// <summary>
        /// Возвращает площадь прямоугольного треугольника. Если треугольник не является прямоугольным или длина одной из сторон меньше или равна 0, вызывает исключение.
        /// </summary>
        /// <param name="a">Первый катет треугольника.</param>
        /// <param name="b">Второй катет треугольника.</param>
        /// <param name="c">Гипотенуза треугольника.</param>
        /// <returns>Вещественное число - площадь треугольника.</returns>
        /// <exception cref="ArgumentException">Если одна сторон меньше или равно нулю или треугольник не является прямоугольным.</exception>
        public static double GetTriangleSquare(double a, double b, double c)
        {
            if (a <= 0 || b <= 0 || c <= 0)
                throw new ArgumentException(SideIsLessThanOrZero);
            else 
                if (Math.Pow(c, 2) != Math.Pow(a, 2) + Math.Pow(b, 2))
                    throw new ArgumentException(TriangleIsNotRight);
            return (a * b) / 2;
        }
    }
}
