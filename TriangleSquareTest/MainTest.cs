﻿using System;
using TriangleSquare;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TriangleSquareTest
{
    [TestClass]
    public class MainTest
    {
        [TestMethod]
        public void Square_WithValidSides_Calc()
        {
            double a = 4.4;
            double b = 3.3;
            double c = 5.5;
            double expectedSquare = (4.4 * 3.3) / 2;

            double actualSquare = Program.GetTriangleSquare(a, b, c);

            Assert.AreEqual<double>(expectedSquare, actualSquare, "Площадь посчитана неверно");
        }

        [TestMethod]
        public void Square_WithNotRightTriangle_ShouldThrowArgumentEx()
        {
            double a = 4.4;
            double b = 3.3;
            double c = 7.7;

            try
            {
                Program.GetTriangleSquare(a, b, c);
            }
            catch (ArgumentException ex)
            {
                StringAssert.Contains(ex.Message, Program.TriangleIsNotRight);
                return;
            }
            Assert.Fail("Исключений для перехвата не возникло.");
        }

        [TestMethod]
        public void Square_WithOneOfSidesLessThanOrZero_ShouldThrowArgumentEx()
        {
            double a = -4.4;
            double b = 0.0;
            double c = 5.5;

            try
            {
                Program.GetTriangleSquare(a, b, c);
            }
            catch (ArgumentException ex)
            {
                StringAssert.Contains(ex.Message, Program.SideIsLessThanOrZero);
                return;
            }
            Assert.Fail("Исключений для перехвата не возникло.");
        }
    }
}
